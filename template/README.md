# {{projectDisplayName}}

This is the {{projectDisplayName}} source.

{{projectDisplayName}} is the Enmotion platform microservice providing functionality for {{projectFunctionality}}.

## Technologies

- This microservice is built using [Moleculer](https://moleculer.services/) on Node.js.
- [NATS](https://nats.io/) is used as the transporter for Enmotion services.
- Caching for Enmotion services are provided with [Redis](https://redis.io/).
- [MongoDB](https://mongodb.com) is used as the database.
- Enmotion is deployed on [Kubernetes](https://kubernetes.io), using [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/docs/quickstart).

## How to setup

Install the following on your local machine before starting.

- [Redis](https://redis.io/topics/quickstart)
- [NATS](https://docs.nats.io/nats-server/installation)
- [MongoDB](https://docs.mongodb.com/manual/installation/#mongodb-community-edition-installation-tutorials)

Afterwards,

1. Clone the project

```sh
git clone git@gitlab.com:enmotion/{{projectName}}.git
```

2. Run yarn install

```sh
cd {{projectName}}
yarn install
```

3. Start development environment

```
yarn dev
```

## Testing

``` bash
# Run unit tests
yarn test

# Run continuous test mode
yarn ci
```

## Run in Docker

- Build Docker image

```bash
$ docker build -t {{projectName}} .
```

- Start container

```bash
$ docker run -d {{projectName}}
```