# Enmotion Moleculer template
Moleculer microservice template for Enmotion services.

## Install
To install use the [moleculer-cli](https://github.com/moleculerjs/moleculer-cli) tool.

```bash
$ moleculer init gitlab:enmotion/microservice-template my-project
```

## NPM scripts
- `npm run dev` - Start service.js with hot-reloading and REPL.
- `npm lint` - Run linting
- `npm run ci` - Start testing in watch mode
- `npm start` - Start service.js in production mode
- `npm test` - Run tests & generate coverage report

## License
This template is available under the [MIT license](https://tldrlegal.com/license/mit-license).

## Contact
Copyright (c) 2019 Enmotion

Copyright (c) 2018 MoleculerJS