"use strict";

module.exports = function(values) {
	return {
		questions: [
			{
				type: "input",
				name: "projectDisplayName",
				message: "Enter project display name",
				default: ""
			},
			{
				type: "input",
				name: "projectFunctionality",
				message: "Enter project functionality",
				default: ""
			}	
		],
		completeMessage: `
To get started:

	cd {{projectName}}
	npm run dev

		`
	};
};
